<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


/** AUTH **/
Auth::routes();

Route::get('/login/{social}', [
    'as' => 'social_redirect',
    'uses' => 'Auth\LoginController@redirectToProvider'
])
    ->where('social', 'facebook|github');
Route::get('/login/{social}/callback', [
    'as' => 'social_callback',
    'uses' => 'Auth\LoginController@handleProviderCallback'
])
    ->where('social', 'facebook|github');
Route::get('/logout', [
    'as' => 'logout',
    'uses' => 'Auth\LoginController@logout'
]);


/** ADMIN **/
Route::prefix('admin')
    ->middleware('auth')
    ->group(
        function() {
            Route::get('/', [
                'as' => 'admin',
                'uses' => 'Admin\IndexController@index'
            ]);
            Route::get('/users', [
                'as' => 'users',
                'uses' => 'Admin\UsersController@index'
            ])
                ->middleware('can:view-users');
        }
    );


/** PRODUCTS **/
Route::prefix('products')
    ->middleware('auth')
    ->group(
        function() {
            Route::get('/', [
                'as' => 'products',
                'uses' => 'ProductController@index'
            ]);
            Route::get('/{id}', [
                'as' => 'product_show',
                'uses' => 'ProductController@show'
            ])
                ->where('id', '[0-9]+');
            Route::post('/add', [
                'as' => 'product_store',
                'uses' => 'ProductController@store'
            ]);
            Route::post('/{id}/edit', [
                'as' => 'product_update',
                'uses' => 'ProductController@update'
            ])
                ->where('id', '[0-9]+')
                ->middleware('can:edit-product,id');
            Route::delete('/{id}/delete', [
                'as' => 'product_delete',
                'uses' => 'ProductController@delete'
            ])
                ->where('id', '[0-9]+')
                ->middleware('can:edit-product,id');
        }
    );
