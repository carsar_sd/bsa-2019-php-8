<?php

use Illuminate\Database\Seeder;
use App\Entity\User;
use App\Entity\Product;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 10)->create()
            ->each(function ($user){
                $user->products()->saveMany(
                    factory(Product::class, mt_rand(1, 5))
                        ->make(['user_id' => null])
                );
            });
    }
}
