@extends('layouts.product')

@section('content')
    <div class="card-body">
        @include('product.errors')
    </div>

    @if($product)
        <div class="card">
            <div class="card-header">
                Current product
                <a style="float: right;" href="{{ route('products') }}">
                    <i class="fa fa-arrow-circle-o-left"></i>&nbsp;Back
                </a>
            </div>
            <div class="card-body">
                <table class="table table-stripped task-table">
                    <thead>
                        <tr class="text-center">
                            <th>ID</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Seller</th>
                            @can('edit-product', $product['id'])
                                <th>..</th>
                                <th>..</th>
                            @else
                                <th>Created</th>
                                <th>Updated</th>
                            @endcan
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="text-center">
                            <form action="{{ route('product_update', ['id' => $product['id']]) }}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('POST') }}
                                <td class="table-text">
                                    {{ $product['id'] }}
                                </td>
                                <td class="table-text">
                                    <input type="text" name="name" value="{{ $product['name'] }}">
                                </td>
                                <td class="table-text">
                                    <input type="text" name="price" value="{{ $product['price'] }}">
                                </td>
                                <td class="table-text">
                                    {{ $product['seller'] }}
                                </td>
                                <td class="table-text">
                                    @can('edit-product', $product['id'])
                                        <button class="btn btn-success"><i class="fa fa-save"></i>&nbsp;Save</button>
                                    @else
                                        {{ $product['created_at'] ?: $product['updated_at'] }}
                                    @endcan
                                </td>
                            </form>
                            <td>
                                @can('edit-product', $product['id'])
                                    <form action="{{ route('product_delete', ['id' => $product['id']]) }}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn btn-danger"><i class="fa fa-remove"></i>&nbsp;Delete</button>
                                    </form>
                                @else
                                    {{ $product['updated_at'] ?: $product['created_at'] }}
                                @endcan
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    @endif
@endsection