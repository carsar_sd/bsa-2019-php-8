@extends('layouts.product')

@section('content')
    <div class="card-body">
        @include('product.errors')

        <form action="{{ route('product_store') }}" method="post" class="form-horizontal">
            {{ csrf_field() }}

            <div class="row">
                <div class="form-group">
                    <label for="product_store" class="col-sm-12 control-label text-left p-0">Product</label>

                    <div class="row" id="product_store">
                        <div class="col-sm-4">
                            <input type="text" name="name" class="form-control">
                        </div>
                        <div class="col-sm-4">
                            <input type="text" name="price" class="form-control">
                        </div>
                        <div class="col-sm-3">
                            <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i>&nbsp;Store</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    @if(count($products) > 0 )
        <div class="card">
            <div class="card-body">
                <table class="table table-stripped task-table">
                    <thead>
                        <tr class="text-center">
                            <th>ID</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Seller</th>
                            <th>Created</th>
                            <th>Updated</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr class="text-center">
                            <td class="table-text">
                                {{ $product['id'] }}
                            </td>
                            <td class="table-text">
                                <a href="{{ route('product_show', ['id' => $product['id']]) }}">
                                    <i class="fa fa-arrow-circle-o-right"></i>&nbsp;{{ $product['name'] }}
                                </a>
                            </td>
                            <td class="table-text">
                                {{ $product['price'] }}
                            </td>
                            <td class="table-text">
                                {{ $product['seller'] }}
                            </td>
                            <td class="table-text">
                                {{ $product['created_at'] ?: $product['updated_at'] }}
                            </td>
                            <td class="table-text">
                                {{ $product['updated_at'] ?: $product['created_at'] }}
                            </td>
                        </tr>
                    @endforeach
                    @if($products->links())
                        <tr>
                            <td class="table-text" colspan="6">
                                {{ $products->links() }}
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    @endif
@endsection