<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('assets/js/vendor/jquery.3.4.0.min.js') }}"></script>
    <script src="{{ asset('assets/js/vendor/bootstrap.4.3.1.min.js') }}"></script>
    <script src="{{ asset('assets/js/product.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('assets/css/vendor/bootstrap.4.3.1.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/vendor/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/product.css') }}" rel="stylesheet">
</head>
<body>
    <div class="container">
        @if (Route::has('login'))
            <div class="top-right links text-right">
                <a href="{{ route('admin') }}">Profile</a>
                &nbsp;|&nbsp;
                <a href="{{ route('logout') }}">Logout</a>
            </div>
        @endif

        @yield('content')
    </div>
</body>
</html>