<?php

namespace App\Http\Controllers;

use App\Policies\ProductPolicy;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Entity\Product;

class ProductController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $itemsPaginated = Product::orderBy('id', 'DESC')->latest()->paginate(10);

        $itemsTransformed = $itemsPaginated
            ->map(function($product) {
                $product['seller'] = $product->user->name;
                return $product;
            });

        $products = new LengthAwarePaginator(
            $itemsTransformed,
            $itemsPaginated->total(),
            $itemsPaginated->perPage(),
            $itemsPaginated->currentPage(), [
                'path' => \Request::url(),
                'query' => [
                    'page' => $itemsPaginated->currentPage()
                ]
            ]
        );

        return view('product.products', compact('products'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);
        $product['seller'] = $product->user->name;

        return view('product.product', ['product' => collect($product)]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name'  => 'required|max:255',
            'price' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect(route('products'))
                ->withInput()
                ->withErrors($validator);
        }

        $product = new Product();

        $product->name = $request->name;
        $product->price = $request->price;
        $product->user_id = Auth::id();

        $product->save();

        return redirect(route('products'));
    }

    /**
     * @param Request $request
     * @param         $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'name'  => 'required|max:255',
            'price' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect(route('product_show', ['id' => $id]))
                ->withInput()
                ->withErrors($validator);
        }

        $data = [
            'name'  => $request->name,
            'price' => $request->price,
        ];

        Product::where('id', $id)->update($data);

        return redirect(route('products'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        Product::where('id', $id)->delete();

        return redirect(route('products'));
    }
}
