<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Entity\User;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::paginate(10);

        return view('admin.users', compact('users'));
    }
}
