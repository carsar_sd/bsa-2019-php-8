<?php

namespace App\Http\Controllers\Auth;

use App\Entity\User;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        logout as performLogout;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo;

    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->redirectTo = config('auth.redirect_products');
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request)
    {
        $this->performLogout($request);
        return redirect(route('login'));
    }

    /**
     * Redirect the user to the Social authentication page.
     *
     * @param string $social
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($social)
    {
        return Socialite::driver($social)->redirect();
    }

    /**
     * Obtain the user information from Social.
     *
     * @param string $social
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($social)
    {
        $userSocial   = Socialite::driver($social)->user();
        $alreadyExist = User::whereEmail($userSocial->email)->first();

        if ($alreadyExist) {
            $user = $alreadyExist;
        } else {
            $user = new User;
            $user->name     = $userSocial->name;
            $user->email    = $userSocial->email;
            $user->password = Hash::make('password');
            $user->save();
        }

        auth()->login($user);

        return redirect(route('products') . '#');
    }
}
