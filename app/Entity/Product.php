<?php

namespace App\Entity;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 *
 * @package App\Entity
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property float $price
 * @property User $user
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Product extends Model
{
    protected $table = 'products';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'price', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getId():int
    {
        return $this->id;
    }

    public function getUserId():int
    {
        return $this->user_id;
    }

    public function getName():string
    {
        return $this->name;
    }

    public function getPrice():float
    {
        return $this->price;
    }

    public function getCreatedAt(): Carbon
    {
        return $this->created_at;
    }

    public function getUpdatedAt(): Carbon
    {
        return $this->updated_at;
    }
}
