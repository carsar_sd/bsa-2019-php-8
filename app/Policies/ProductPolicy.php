<?php

namespace App\Policies;

use App\Entity\Product;
use App\Entity\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param int $product_id
     * @return bool
     */
    public function edit(User $user, int $product_id):bool
    {
        $product = Product::findOrFail($product_id);

        return (bool) $user->getIsAdmin() || $user->id == $product->user_id;
    }
}
